window.game = Game();
window.onload = function(){
    const form = document.forms['dataform'];
    var queries = window.location.href.split('?');

    if (queries.length>1) {
        queries = queries[queries.length-1];
        for (const txt of queries.split('&')) {
            const vals = txt.split('=');
            if (vals[0]=='light' || vals[0]=='lightz') {
                form[vals[0]].checked = vals[1]=='on';
            } else {
                form[vals[0]].value = vals[1];
            }
        }
    }
    loadgame();
};

function loadgame() {
    const form = document.forms['dataform'];
    game.load(
        islight=form.light.checked,
        iszap=form.lightz.checked,
        lightx=parseFloat(form.lightx.value),
        wallx=parseFloat(form.wallx.value),
        wallb=parseFloat(form.wallb.value)
    ); game.start();
}

function Game() {
    const FPS = 30;
    const NUM_FLIES = 100;
    var TIMER = null;

    const stats = { dead:[], house:[] };

    const W = 700;
    const H = 300;
    const SPEED = 0.03;

    var WALL_X, WALL_BUFFER;
    const WALL_SPACE = 0.005;
    var LIGHT, LIGHT_X, LIGHT_Z;

    const LIGHT_Y = 0.5;
    const LIGHT_S = 0.05;
    const lightsize = LIGHT_S*2*Math.min(W,H);

    var flies = [];
    function newfly() {
        var ele = document.createElement('div');
        ele.className = 'fly';

        ele.dead = false;
        ele.x = Math.random()*(WALL_X-WALL_BUFFER);
        ele.y = Math.random();
        ele.pos = Math.random();
        return ele;
    }

    function drawele(ele) {
        ele.style.top = (ele.y*H - 2)+'px';
        ele.style.left = (ele.x*W - 2)+'px';
        if (ele.dead) ele.style.background = '#f00';
    }

    function update() {
        function move(ele) {
            const deg = ele.pos*Math.PI*2;
            ele.x += Math.cos(deg)*(SPEED/FPS);
            ele.y += Math.sin(deg)*(SPEED/FPS);
        }

        function flip(ele) {
            fly.pos = (fly.pos+Math.PI)%(Math.PI*2);
        }

        function lightdist(ele) {
            if (!LIGHT) return 0;
            const dx = W*(ele.x-LIGHT_X);
            const dy = H*(ele.y-LIGHT_Y);
            return Math.sqrt(dx*dx + dy*dy);
        }

        function updatestats(cnt0,cnt1) {
            stats.dead.push(cnt0);
            stats.house.push(cnt1);

            const dead = stats.dead;
            const pts = ['M 0,'+H+' L'];
            for (var i=0;i<dead.length;i++) {
                pts.push(parseInt((W*i)/dead.length)
                    +','+parseInt(H*(1-dead[i])));
            }
            document.getElementById('deadflies')
                .setAttribute('d', pts.join(' '));
            document.getElementById('deadfly').innerText =
                parseInt(dead[dead.length-1]*100);

            const hse = stats.house;
            const pts0 = ['M 0,'+H+' L'];
            for (var i=0;i<hse.length;i++) {
                pts0.push(parseInt((W*i)/hse.length)
                    +','+parseInt(H*(1-hse[i])));
            }
            document.getElementById('houseflies')
                .setAttribute('d', pts0.join(' '));
            document.getElementById('housefly').innerText =
                parseInt(hse[hse.length-1]*100);
        }

        var flies_hse = 0;
        var flies_dead = 0;
        for (var fly of flies) {
            if (fly.x > WALL_X) flies_hse++;
            if (fly.dead) {
                flies_dead++;
                continue;
            }

            const d1 = lightdist(fly);
            move(fly);

            if (fly.x<0 || fly.x>1 || fly.y<0 || fly.y>1) {
                fly.x = Math.max(Math.min(1, fly.x), 0);
                fly.y = Math.max(Math.min(1, fly.y), 0);
                flip(fly);
            }

            if ( !(fly.y<WALL_BUFFER || fly.y>(1-WALL_BUFFER)) &&
                Math.abs(fly.x-WALL_X)<WALL_SPACE
            ) {
                if (fly.x<WALL_X) fly.x = WALL_X-WALL_SPACE;
                if (fly.x>WALL_X) fly.x = WALL_X+WALL_SPACE;
            }

            // if fly gets closer to light, keep moving in direction
            if (LIGHT) {
                const d2 = lightdist(fly);
                var frac = d2>0.1 ?6 :10;
                if (d1>d2) frac=20;
                fly.pos += (Math.random()-0.5)/frac;
                if (LIGHT_Z && d2<lightsize/2) fly.dead = true;
            } else {
                fly.pos += (Math.random()-0.5)/8;
            }

            drawele(fly);
        }

        updatestats(
            flies_dead/flies.length,
            flies_hse/flies.length);
    }

    function start() {
        if (TIMER) clearInterval(TIMER);
        TIMER = setInterval(update, 1000/FPS);
    }

    function load(islight=true,iszap=true,lightx=0.3,wallx=0.8,wallb=0.05) {
        LIGHT = islight;
        LIGHT_Z = iszap;
        LIGHT_X = lightx;

        WALL_X = wallx;
        WALL_BUFFER = wallb;

        const canvas = document.getElementById('game');
        canvas.style.width = W+'px';
        canvas.style.height = H+'px';
        canvas.innerHTML = '';

        const wall = document.createElement('div');
        wall.className = 'wall';
        wall.style.left = (WALL_X*W)+'px';
        wall.style.top = (WALL_BUFFER*100)+'%';
        wall.style.height = ((1-(2*WALL_BUFFER))*H)+'px';
        canvas.appendChild(wall);

        for (const svg of document.getElementsByTagName('svg')) {
            svg.style.width = W+'px';
            svg.style.height = H+'px';
            svg.setAttribute('viewBox', '0 0 '+W+' '+H);
        }

        if (LIGHT) {
            const light = document.createElement('div');
            light.className = 'light';
            light.style.left = ((LIGHT_X*W)-(lightsize/2))+'px';
            light.style.top = ((LIGHT_Y*H)-(lightsize/2))+'px';
            light.style.width = lightsize+'px';
            light.style.height = lightsize+'px';
            canvas.appendChild(light);
        }

        flies = [];
        for (var i=0; i<NUM_FLIES; i++) {
            const fly = newfly();
            flies.push(fly);
            canvas.appendChild(fly);
        }

        stats.dead = [];
        stats.house = [];
    }
    return { load, start, stats };
}
